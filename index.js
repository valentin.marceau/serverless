function main(data) {
  if (data.data) {
    var archetype = data.data;
    var string = "le monstre "+data.monster.name+" est un monstre de niveau "+data.monster.level+" doté de "+data.monster.atk+" points d'attaque et de "+data.monster.def+" points de défense. Les dernier supports de son archétype sont : ";
    archetype.forEach(function(item, index, array) {
      if (index === 0){
        string += item.name;
     }
     else if (index === array.length - 1){
       string += " et "+item.name;
    }
    else {
      string+= ', '+item.name
    }
    });
    return string;
  }
  else {
    return "Ce monstre n'existe pas";
  }
}
