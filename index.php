<?php
function main($data = null) {
  $infos = json_decode($data);
  $curl = curl_init();

  var_dump($infos);

  curl_setopt_array($curl, array(
    CURLOPT_URL => "https://db.ygoprodeck.com/api/v7/cardinfo.php?archetype=".str_replace(' ', '%20', $infos->monster->archetype)."&num=5&offset=0&sort=new",
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => "",
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 30,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => "GET",
    CURLOPT_HTTPHEADER => array(
      "cache-control: no-cache",
      "postman-token: 3df089ab-2555-2880-5beb-ecce2514fb30"
    ),
  ));

  $response = curl_exec($curl);
  $err = curl_error($curl);

  curl_close($curl);

  if (!json_decode($response)->data) {
    return $response;
  } else {
    $data_to_send[] = json_decode($response);
    $data_to_send[] = $infos;
    $data_to_send = json_encode($data_to_send);
    var_dump($data_to_send);
  }
}
