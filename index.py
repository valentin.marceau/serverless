import requests
def main(data)
    data = data.replace(' ', '%20')
    response = requests.get("https://db.ygoprodeck.com/api/v7/cardinfo.php?name="+data)

    if response.status_code == 200:
        data = response.json()
        name = str(data["data"][0]["name"])
        archetype = str(data["data"][0]["archetype"])
        price = str(data["data"][0]["card_prices"][0]["amazon_price"])
        atk = str(data["data"][0]["atk"])
        defense = str(data["data"][0]["def"])
        level = str(data["data"][0]["level"])
        json = "{'monster':[{'name': '"+name+"', 'archetype': '"+archetype+"', 'price': "+price+", 'atk': "+atk+", 'def': "+defense+", 'level': "+level+"}]}"
        return json
    else:
        return "{'error': 'not a existing card'}"
